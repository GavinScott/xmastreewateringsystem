# README #

This repository contains the circuit diagram and Arduino sketch for an automatic Christmas-tree watering system. Click [here](TBD) for the YouTube video about this project.

This system is powered by an Arduino nano which periodically checks the water level in the tree using a conductivity-based water sensor. When it detects that the water level has fallen too low, it turns on a water pump to top up the tree’s water supply from a nearby 5-gallon bucket. Rather than just point the end of the tube at the base of the tree, I thought it’d be fun to have the water spew out of the mouth of a 3D printed Santa Claus.

The water sensor I used is corrosive and will almost definitely fall apart before the holiday season is over, and could very well be bad for the tree. I just used it because I happened to have it on hand; I’d recommend a capacitive water sensor (there’s a link to one in the parts list of the YouTube video), since those don’t have exposed copper and won’t have the same issues.
