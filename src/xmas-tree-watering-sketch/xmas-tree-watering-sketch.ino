#define STATE_HAPPY 1
#define STATE_FILL 2
#define STATE_ERROR 3

int waterSensorPin = A5;
int ledPin = 13;
int relayPin = 9;

int checkDelay = 1000;
int fillTime = 30000;
int fillBlinkDelay = 200;
int resistanceThreshold = 100;
int fillCounter = 0;

int state = STATE_HAPPY;

void setup() {
  Serial.begin(9600);
  Serial.println("Starting");
  pinMode(ledPin, OUTPUT);
  pinMode(relayPin, OUTPUT);
  digitalWrite(ledPin, LOW);
  digitalWrite(relayPin, LOW);
}

bool needsWater() {
  int value = analogRead(waterSensorPin);
  Serial.print("Read value: ");
  Serial.println(value);
  return value <= resistanceThreshold;
}

void loop() {
  if (state == STATE_ERROR) {
    digitalWrite(ledPin, HIGH);
  } else if (state == STATE_FILL) {
    Serial.println("Refilling water supply");
    digitalWrite(relayPin, HIGH);
    if (fillCounter++ % 2 == 0) {
      digitalWrite(ledPin, HIGH);
    } else {
      digitalWrite(ledPin, LOW);
    }
    delay(fillBlinkDelay);
    if (fillCounter * fillBlinkDelay >= fillTime) {
      digitalWrite(relayPin, LOW);
      digitalWrite(ledPin, LOW);
      if (needsWater()) {
        Serial.println("No water after refill, assuming something is wrong");
        state = STATE_ERROR;
      } else {
        state = STATE_HAPPY;
      }
    }
  } else {
    Serial.println("Checking water level");
    fillCounter = 0;
    if (needsWater()) {
      Serial.println("Water level low, triggering refill");
      state = STATE_FILL;
    } else {
      Serial.println("Water level fine");
    }
    delay(checkDelay);
  }
}

